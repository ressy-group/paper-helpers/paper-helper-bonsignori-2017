# DH270/DH272/DH475 antibody sequences gathered from Bonsignori 2017

Staged induction of HIV-1 glycan–dependent broadly neutralizing antibodies.
Mattia Bonsignori et al.
Science Translational Medicine, Vol 9, Issue 381
<https://doi.org/10.1126/scitranslmed.aai7514>

 * Subject: CH848
 * Antibody specificity: V3 glycan
 * Antibody lineages: DH270, DH272, DH475
 * GenBank entries:
   * KY354938 to KY354963
   * KY347498 to KY347701
 * PDB:
   * UCA1: 5U0R
   * UCA3: 5U15
   * DH270.1: 5U0U
   * DH270.3: 5TPL
   * DH270.5: 5TPP
   * DH270.6: 5TQA
   * DH272: 5TRP
