with open("from-paper/genbank_accessions.txt") as f_in:
    GB_ACCESSIONS = [line.strip() for line in f_in]
with open("from-paper/pdb_accessions.txt") as f_in:
    PDB_ACCESSIONS = [line.strip() for line in f_in]

TARGET_GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS)
TARGET_PDB_FASTA = expand("from-pdb/{acc}.fasta", acc=PDB_ACCESSIONS)

rule all:
    input: expand("output/seqs_{thing}.csv", thing=["paired", "ngs"]) + expand("parsed/figS15{panel}.fasta", panel=["A", "B"])

rule make_ngs_sheet:
    output: "output/seqs_ngs.csv"
    input: "parsed/genbank.csv"
    shell: "egrep 'Category|,NGS,' {input} > {output}"

rule make_pairs_sheet:
    output: "output/seqs_paired.csv"
    input: "parsed/genbank.csv"
    shell: "python scripts/make_pairs_sheet.py {input} {output}"

rule all_pdb_fasta:
    input: TARGET_PDB_FASTA

rule download_pdb_fa:
    output: "from-pdb/{acc}.fasta"
    shell: "python scripts/download_ncbi.py protein {wildcards.acc} {output}"

rule parse_s15:
    output: "parsed/fig{fig}.fasta"
    input: "from-paper/fig{fig}.txt"
    shell: "python scripts/parse_s15.py {input} {output}"

rule make_seqs_sheet:
    output: "parsed/genbank.csv"
    input: TARGET_GBF_FASTA
    shell: "python scripts/make_genbank_sheet.py {input} > {output}"

rule all_gbf_fasta:
    input: TARGET_GBF_FASTA

rule download_gbf_fa:
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_ncbi.py nucleotide {wildcards.acc} {output}"
