#!/usr/bin/env python

"""
Make CSV of antibody seq attributes from downloaded GenBank files.
"""

import re
import sys
from collections import defaultdict
from csv import DictReader, DictWriter
from Bio import SeqIO

def parse_seq_desc(txt):
    pats = {
        "NGS":      r"([A-Z0-9.]+) .*isolate (DH[0-9]+[0-9_.]+VH) anti-HIV-1 .*(heavy|lambda|kappa) .*",
        "inferred": r"([A-Z0-9.]+) .*isolate (DH[0-9]+[. ][UCAI]+[0-9]*) immunoglobulin .*(heavy|lambda|kappa) .*",
        "mAb":      r"([A-Z0-9.]+) .*isolate (DH[0-9.]+) immunoglobulin .*(heavy|lambda|kappa) .*"}
    matches = [(cat, re.match(pat, txt)) for cat, pat in pats.items()]
    matches = [pair for pair in matches if pair[1]]
    if len(matches) > 1:
        cats = [p[0] for p in matches]
        raise ValueError(f"Ambiguous parsing for \"{txt}\" ({cats})")
    if len(matches) < 1:
        raise ValueError(f"Can't parse \"{txt}\"")
    cat, match = matches[0]
    grps = match.groups()
    keys = ["Accession", "Antibody", "Locus"]
    fields = {k: v for k, v in zip(keys, grps)}
    fields["Locus"] = "IG" + fields["Locus"][0].upper()
    fields["Chain"] = "heavy" if fields["Locus"] == "IGH" else "light"
    fields["Category"] = cat
    fields["Lineage"] = re.sub("[ .].*", "", fields["Antibody"])
    return fields

FIELDS = ["Antibody", "Accession", "Lineage", "Category", "Chain", "Locus", "Seq"]

def make_seqs_sheet(fastas):
    mabs = []
    for fasta in fastas:
        for record in SeqIO.parse(fasta, "fasta"):
            fields = parse_seq_desc(record.description)
            fields["Seq"] = str(record.seq)
            mabs.append(fields)
    mabs.sort(key = lambda row: (row["Lineage"], row["Category"], row["Accession"]))
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(mabs)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:])
