#!/usr/bin/env python

"""
Make CSV of antibody seq attributes from downloaded GenBank files.
"""

import sys
from collections import defaultdict
from csv import DictReader, DictWriter

def make_pairs_sheet(csv_in, csv_out):
    fields = ["Antibody", "Lineage", "Category", "Loci",
        "HeavyAccession", "HeavySeq", "LightAccession", "LightSeq"]
    with open(csv_in) as f_in, open(csv_out, "w") as f_out:
        mabs = defaultdict(dict)
        for row in DictReader(f_in):
            if row["Category"] == "NGS":
                continue
            key = row["Antibody"]
            col_acc = row["Chain"].capitalize() + "Accession"
            col_seq = row["Chain"].capitalize() + "Seq"
            mabs[key]["Antibody"] = row["Antibody"]
            mabs[key][col_acc] = row["Accession"]
            mabs[key][col_seq] = row["Seq"]
            mabs[key]["Lineage"] = row["Lineage"]
            mabs[key]["Category"] = row["Category"]
            mabs[key]["Loci"] = mabs[key].get("Loci", set())
            mabs[key]["Loci"].add(row["Locus"])
        for vals in mabs.values():
            vals["Loci"] = "+".join(sorted(list(vals["Loci"])))
        mabs = list(mabs.values())
        writer = DictWriter(
            f_out,
            fieldnames=fields,
            lineterminator="\n")
        writer.writeheader()
        writer.writerows(mabs)

if __name__ == "__main__":
    make_pairs_sheet(sys.argv[1], sys.argv[2])
