#!/usr/bin/env python

# Adapted from bonsignori 2011, pancera 2010, and changela 2011 repos

"""
Configure Figure S15 panel text into FASTA.
"""

import re
import sys
from collections import defaultdict

def glom_chunk(entries, chunk):
    if entries:
        pad = max(len(val) for val in entries.values())
        pad = "-" * pad
    else:
        pad = ""
    for key in chunk:
        # to handle implicit gaps in alignments
        if key not in entries:
            entries[key] = pad
        entries[key] += chunk[key]
    chunk.clear()

def proc_seq_line(chunk, line):
    match = re.search("([^ ]+) +(.*)", line)
    seqid, seqparts = match.groups()
    seqparts = seqparts.split(" ")
    seqparts = [x for x in seqparts if re.match("[-A-Za-z.]+", x)]
    seq = "".join(seqparts)
    chunk[seqid] = seq

def parse_s2(path, path_out):
    entries = defaultdict(str)
    chunk = {}
    newchunk = False
    with open(path) as f_in:
        for line in f_in:
            if line == "\n":
                # skip blank lines
                continue
            if line.startswith(" "):
                # we've hit a new chunk; process the old one
                glom_chunk(entries, chunk)
                continue
            proc_seq_line(chunk, line)
        # process the last chunk too
        glom_chunk(entries, chunk)
    with open(path_out, "w") as f_out:
        for key, val in entries.items():
            f_out.write(f">{key}\n{val}\n")

if __name__ == "__main__":
    parse_s2(sys.argv[1], sys.argv[2])
